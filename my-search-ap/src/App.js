
import React, { useEffect, useState } from "react";

import axios from 'axios';
function App() {
  const [keyword, setKeyword] = useState("");
  const [ads,setAds] = useState([]);
  const handleKeywordChange = (event) => {
    setKeyword(event.target.value);
  };
  useEffect(() => {
    axios.get('https://api.example.com/data')
      .then(response => {
        setAds(response.data);
      })
      .catch(error => {
        console.error('Error fetching data:', error);
      });
  }, []);

  return (
    <div>
      <input type="text" value={keyword} onChange={handleKeywordChange} />
      <div className="ads-grid">
        {ads.map((ad) => (
          <div key={ad._id} className="ad">
            <img src={ad.imageUrl} alt="" />
            <h3>{ad.primaryText}</h3>
            <p>{ad.description}</p>
            <a href={ad.CTA}>Learn More</a>
            <span>{ad.company.name}</span>
          </div>
        ))}
      </div>
    </div>
  );
}

export default App;

